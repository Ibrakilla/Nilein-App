import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { App, MenuController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RestProvider } from "../../providers/rest/rest";
/**
 * Generated class for the NewClaimPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-user-claim",
  templateUrl: "user-claim.html"
})
export class UserClaimPage {
  claims: any;
  pendingclaims: any;
  segment: any;
  apiPendingClaims = "http://elnileinapi.com/api/policies/getclaims";
  apiUserClaims = "http://elnileinapi.com/api/policies/GetPendingClaims";
  checkData = false;
  checkData1 = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    private menu: MenuController,
    public http: HttpClient,
    public appCtrl: App,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage
  ) {
    let loader = this.loadingCtrl.create({
      spinner: "hide",
      content: `<img src="assets/img/loadin-gif.gif" />`,
      duration: 5000
    });
    loader.present();
    this.segment = "development";
    this.storage.get("PolicyNo").then(PolicyNo => {
      this.storage.get("SNo").then(SNo => {
        this.http
          .get(
            this.apiPendingClaims +
              "?ClientID=" +
              SNo +
              "&PolicyNo=" +
              PolicyNo +
              "",
            {
              headers: new HttpHeaders().set("Content-Type", "application/json")
            }
          )
          .subscribe(
            res => {
              if (Object.keys(res).length == 0) {
                this.checkData = false;
              } else {
                this.checkData = true;
              }
              console.log(res);
              this.claims = res;
            },
            err => {
              this.checkData = false;
              console.log(err);
            }
          );

        this.http
          .get(this.apiUserClaims + "?ClientNo=" + SNo, {
            headers: new HttpHeaders().set("Content-Type", "application/json")
          })
          .subscribe(
            res => {
              if (Object.keys(res).length == 0) {
                this.checkData1 = false;
              } else {
                this.checkData1 = true;
              }
              console.log(res);
              this.pendingclaims = res;
              loader.dismiss();
            },
            err => {
              this.checkData1 = false;
              console.log(err);
              loader.dismiss();
            }
          );
      });
    });
  }
  doRefresh(refresher) {
    this.storage.get("PolicyNo").then(PolicyNo => {
      this.storage.get("SNo").then(SNo => {
        this.http
          .get(
            this.apiPendingClaims +
              "?ClientID=" +
              SNo +
              "&PolicyNo=" +
              PolicyNo +
              "",
            {
              headers: new HttpHeaders().set("Content-Type", "application/json")
            }
          )
          .subscribe(
            res => {
              if (Object.keys(res).length == 0) {
                this.checkData = false;
              } else {
                this.checkData = true;
              }
              console.log(res);
              this.claims = res;
            },
            err => {
              this.checkData = false;
              console.log(err);
            }
          );

        this.http
          .get(this.apiUserClaims + "?ClientNo=" + SNo, {
            headers: new HttpHeaders().set("Content-Type", "application/json")
          })
          .subscribe(
            res => {
              if (Object.keys(res).length == 0) {
                this.checkData1 = false;
              } else {
                this.checkData1 = true;
              }
              console.log(res);
              this.pendingclaims = res;
              refresher.complete();
            },
            err => {
              this.checkData1 = false;
              console.log(err);
              refresher.complete();
            }
          );
      });
    });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad PolicyPage");
  }
  Logout() {
    this.storage.clear();
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }
  GoBack() {
    this.storage.set("LoggedIn", true);
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }
}
