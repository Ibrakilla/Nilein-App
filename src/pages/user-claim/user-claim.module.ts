import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserClaimPage } from './user-claim';

@NgModule({
  declarations: [
    UserClaimPage,
  ],
  imports: [
    IonicPageModule.forChild(UserClaimPage),
  ],
})
export class UserClaimPageModule {}
