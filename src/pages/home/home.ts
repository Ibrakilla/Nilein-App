/** Represents a Component of Components page. */

/** Import Modules */
import { Component } from "@angular/core";
import {
  NavController,
  IonicPage,
  ModalController,
  MenuController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser";
declare var window;
@IonicPage()
@Component({
  selector: "home",
  templateUrl: "home.html"
})
export class HomeComponent {
  logged: string;
  url: string;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private menu: MenuController,
    private inAppBrowser: InAppBrowser
  ) {
    this.navCtrl = navCtrl;
    storage.set("name", "Max");
    this.menu.swipeEnable(true);
  }
  /**
   * gotoTargetComponent(value)
   * This function works on click event
   * when user click any component its get the value of this specific event and
   * NavController set this component and will take user to that component
   */
  chat() {
    const options: InAppBrowserOptions = {
      zoom: "no"
    };
    // Opening a URL and returning an InAppBrowserObject
    this.inAppBrowser.create("https://lc.chat/now/9453930/", "_self", options);
  }
  callIT(passedNumber) {
    //You can add some logic here
    window.location = passedNumber;
  }
  gotoTargetComponent(value) {
    if (value === "Category") {
      // When value is Profile
      this.navCtrl.push("CategoryComponent");
    } else if (value === "AboutUs") {
      // When value is About Us
      this.navCtrl.push("AboutUsComponent");
    } else if (value === "claims") {
      // When value is About Us
      this.navCtrl.push("ClaimsComponent");
    } else if (value === "Location") {
      // When value is About Us
      this.navCtrl.push("LocationComponent");
    } else if (value === "ContactUs") {
      // When value is About Us
      this.navCtrl.push("ContactUsComponent");
    } else if (value === "Complaints") {
      // When value is About Us
      this.navCtrl.push("ComplaintsComponent");
    } else if (value === "login") {
      // When value is About Us
      this.storage.get("LoggedIn").then(LoggedIn => {
        console.log("LoggedIn: " + LoggedIn);
        if (LoggedIn) {
          this.navCtrl.setRoot("PortalTabsPage");
        } else {
          console.log("LoggedIn: " + LoggedIn);
          this.navCtrl.push("LoginComponent");
        }
      });
    }
  }
}
