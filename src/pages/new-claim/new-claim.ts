import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from "ionic-angular";
import { App } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { RestProvider } from "../../providers/rest/rest";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import {
  ToastController,
  LoadingController,
  MenuController
} from "ionic-angular";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { FileUploadOptions } from "@ionic-native/transfer";
import { Platform } from "ionic-angular";
/**
 * Generated class for the NewClaimPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-new-claim",
  templateUrl: "new-claim.html"
})
export class NewClaimPage implements OnInit {
  public photo1: any;
  public base64Image1: string;
  public buttonImage1: boolean = true;
  public photo2: any;
  public base64Image2: string;
  public buttonImage2: boolean = true;
  public photo3: any;
  public base64Image3: string;
  public buttonImage3: boolean = true;
  public error: string;
  public ClaimSNo: any;
  msgApi: any;
  ClientName: string;
  PolicyNo: string;
  PeriodFrom: string;
  PeriodTo: string;
  PlateNo: string;
  PolicyHolder: string;
  SNo: string;
  myForm: any;
  userInfo = { myDate: "", location: "", comment: "" };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public appCtrl: App,
    private storage: Storage,
    public formBuilder: FormBuilder,
    public restProvider: RestProvider,
    private alertCtrl: AlertController,
    private camera: Camera,
    private readonly loadingCtrl: LoadingController,
    private menu: MenuController,
    private readonly toastCtrl: ToastController,
    private transfer: FileTransfer,
    public platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad NewClaimPage");
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    this.storage.get("ClaimSent").then(ClaimSent => {
      console.log("ClaimSent: " + ClaimSent);
      if (ClaimSent) {
        this.Clear();
      }
    });

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
  }
  Logout() {
    this.storage.set("LoggedIn", false);
    this.storage.clear();
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }
  GoBack() {
    this.storage.set("LoggedIn", true);
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }
  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الرسالة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "خطأ في الإرسال, الرجاء اعادة المحاولة",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  presentSuccessToast2() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الصور بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast2() {
    let toast = this.toastCtrl.create({
      message: "خطأ في ارسال الصور, الرجاء اعادة المحاولة",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  ngOnInit(): any {
    this.photo1 = [];
    this.buttonImage1 = true;
    this.photo2 = [];
    this.buttonImage2 = true;
    this.photo3 = [];
    this.buttonImage3 = true;
    this.myForm = this.formBuilder.group({
      myDate: ["", [Validators.required, Validators.minLength(1)]],
      location: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(1000),
          this.locationValidator.bind(this)
        ]
      ],
      comment: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(1000),
          this.commentValidator.bind(this)
        ]
      ]
    });
  }

  isValid(field: string) {
    let formField = this.myForm.get(field);
    return formField.valid || formField.pristine;
  }

  locationValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }

  commentValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }

  myDateValidator(control: FormControl): { [s: string]: boolean } {
    if (
      !control.value.match(
        "/^(0[1-9]|1[0-2])/(0[1-9]|1d|2d|3[01])/(19|20)d{2}$/"
      )
    ) {
      return {};
    }
  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  getClaimSNO(claimSNo) {
    this.storage.set("ClaimSNo", claimSNo).then(ClientName => {});
  }

  onSubmit() {
    this.storage
      .set("UserClaimDate", this.userInfo.myDate)
      .then(UserClaimDate => {
        this.storage
          .set("UserClaimLocation", this.userInfo.location)
          .then(UserClaimLocation => {
            this.storage
              .set("UserClaimComment", this.userInfo.comment)
              .then(UserClaimComment => {
                this.storage.set("ClaimSent", false).then(UserClaimComment => {
                  this.navCtrl.push("NewClaimPage2");
                });
              });
          });
      });
  }

  takePhoto1() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage1 = false;
        this.base64Image1 = "data:image/jpeg;base64," + imageData;
        this.photo1.push(this.base64Image1);
        this.photo1.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto1(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage1 = true;
            this.photo1 = [];
            this.base64Image1 = "";
            this.photo1.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto2() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage2 = false;
        this.base64Image2 = "data:image/jpeg;base64," + imageData;
        this.photo2.push(this.base64Image2);
        this.photo2.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto2(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage2 = true;
            this.photo2 = [];
            this.base64Image2 = "";
            this.photo2.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto3() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage3 = false;
        this.base64Image3 = "data:image/jpeg;base64," + imageData;
        this.photo3.push(this.base64Image3);
        this.photo3.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto3(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage3 = true;
            this.photo3 = [];
            this.base64Image3 = "";
            this.photo3.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  uploadFile(claimsNo) {
    let loader = this.loadingCtrl.create({
      content: "جاري ارسال الصور..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: "file",
      fileName: "image1.jpg",
      chunkedMode: false,
      headers: {}
    };
    fileTransfer
      .upload(
        this.photo1[0],
        "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" + claimsNo,
        options
      )
      .then(
        data => {
          console.log("Uploaded Image 1");
          const fileTransfer: FileTransferObject = this.transfer.create();
          let options: FileUploadOptions = {
            fileKey: "file",
            fileName: "image2.jpg",
            chunkedMode: false,
            headers: {}
          };
          fileTransfer
            .upload(
              this.photo2[0],
              "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                claimsNo,
              options
            )
            .then(
              data => {
                console.log("Uploaded Image 2");
                const fileTransfer: FileTransferObject = this.transfer.create();
                let options: FileUploadOptions = {
                  fileKey: "file",
                  fileName: "image3.jpg",
                  chunkedMode: false,
                  headers: {}
                };
                fileTransfer
                  .upload(
                    this.photo3[0],
                    "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                      claimsNo,
                    options
                  )
                  .then(
                    data => {
                      this.Clear();
                      loader.dismiss();
                      this.presentSuccessToast2();
                      console.log("Uploaded Image 3");
                      console.log(data + " Uploaded Successfully");
                      //
                      // this.presentToast("Image uploaded successfully");
                    },
                    err => {
                      loader.dismiss();
                      this.presentErrorToast2();
                      console.log(err);
                      //
                      //  this.presentToast(err);
                    }
                  );
              },
              err => {
                console.log("Uploaded Image 2");
                const fileTransfer: FileTransferObject = this.transfer.create();
                let options: FileUploadOptions = {
                  fileKey: "file",
                  fileName: "image3.jpg",
                  chunkedMode: false,
                  headers: {}
                };
                fileTransfer
                  .upload(
                    this.photo3[0],
                    "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                      claimsNo,
                    options
                  )
                  .then(
                    data => {
                      this.Clear();
                      loader.dismiss();
                      this.presentSuccessToast2();
                      console.log("Uploaded Image 3");
                      console.log(data + " Uploaded Successfully");
                      //
                      // this.presentToast("Image uploaded successfully");
                    },
                    err => {
                      loader.dismiss();
                      this.presentErrorToast2();
                      console.log(err);
                      //
                      // this.presentToast(err);
                    }
                  );
              }
            );
        },
        err => {
          {
            console.log("Uploaded Image 1");
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
              fileKey: "file",
              fileName: "image2.jpg",
              chunkedMode: false,
              headers: {}
            };
            fileTransfer
              .upload(
                this.photo2[0],
                "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                  claimsNo,
                options
              )
              .then(
                data => {
                  console.log("Uploaded Image 2");
                  const fileTransfer: FileTransferObject = this.transfer.create();
                  let options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: "image3.jpg",
                    chunkedMode: false,
                    headers: {}
                  };
                  fileTransfer
                    .upload(
                      this.photo3[0],
                      "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                        claimsNo,
                      options
                    )
                    .then(
                      data => {
                        this.Clear();
                        loader.dismiss();
                        this.presentSuccessToast2();
                        console.log("Uploaded Image 3");
                        console.log(data + " Uploaded Successfully");
                        //
                        // this.presentToast("Image uploaded successfully");
                      },
                      err => {
                        loader.dismiss();
                        this.presentErrorToast2();
                        console.log(err);
                        //
                        //  this.presentToast(err);
                      }
                    );
                  // this.Clear();
                  console.log("Uploaded Image 2");
                  console.log(data + " Uploaded Successfully");
                  //
                  //  this.presentToast("Image uploaded successfully");
                },
                err => {
                  console.log("Uploaded Image 2");
                  const fileTransfer: FileTransferObject = this.transfer.create();
                  let options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: "image3.jpg",
                    chunkedMode: false,
                    headers: {}
                  };
                  fileTransfer
                    .upload(
                      this.photo3[0],
                      "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                        claimsNo,
                      options
                    )
                    .then(
                      data => {
                        //   this.Clear();
                        console.log("Uploaded Image 3");
                        console.log(data + " Uploaded Successfully");
                        this.Clear();
                        loader.dismiss();
                        this.presentSuccessToast2();
                        // this.presentToast("Image uploaded successfully");
                      },
                      err => {
                        //  this.Clear();
                        console.log(err);
                        loader.dismiss();
                        this.presentErrorToast2();
                        //  this.presentToast(err);
                      }
                    );
                  //  this.Clear();
                  console.log(err);
                  //
                  //   this.presentToast(err);
                }
              );
          }
        }
      );
  }

  Clear() {
    this.ngOnInit();
    this.photo1 = [];
    this.photo2 = [];
    this.photo3 = [];
    this.base64Image1 = "";
    this.base64Image2 = "";
    this.base64Image3 = "";
    this.buttonImage1 = true;
    this.buttonImage2 = true;
    this.buttonImage3 = true;
    this.userInfo = { myDate: "", location: "", comment: "" };
  }
}
