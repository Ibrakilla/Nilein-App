/** Category Service */
import { Injectable } from '@angular/core';
@Injectable()
export class CategoryService {
  constructor() {}
  /** All Categories */
  getAllCategories() {
    return [{
      id: 1,
      name: 'التأمين الطبي',
      image: 'assets/img/medical.jpg'
    }, {
      id: 2,
      name: 'التأمين البحري',
      image: 'assets/img/Marine.jpg'
    }, {
      id: 3,
      name: 'تأمين الحريق',
      image: 'assets/img/fire.jpg'
    }, {
      id: 4,
      name: 'تأمين السيارات',
      image: 'assets/img/car.jpg'
    }, {
      id: 5,
      name: 'التأمين التكافلي',
      image: 'assets/img/life.jpg'
    }, {
      id: 6,
      name: 'التأمين الهندسي',
      image: 'assets/img/engineering.jpg'
    }, {
      id: 7,
      name: 'تأمين الحوادث المتنوعة',
      image: 'assets/img/accident.jpg'
    }, {
      id: 8,
      name: 'تأمين السفر',
      image: 'assets/img/travel.jpg'
    },
    {
      id: 9,
      name: 'التأمين الزراعي',
      image: 'assets/img/farm.jpg'
    },
    {
      id: 10,
      name: 'تأمين الثروة الحيوانية',
      image: 'assets/img/animals.jpg'
    }, ]
  }
}
