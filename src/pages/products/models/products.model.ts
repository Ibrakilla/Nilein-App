/** Product Model */
export class ProductModel {
  id: number;
  name: string;
  description: string;
  description2: string;
  description3: string;
  description4: string;
  description5: string;
  description6: string;
  categoryId: number;
  image: string;
  price: number;
  availability: string;
}
