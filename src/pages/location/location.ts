/** Represents a Component of AboutUs. */

/** Imports Modules */
import { Component, ViewChild, ElementRef } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";
import { Platform } from "ionic-angular/platform/platform";

declare var google: any;

@IonicPage()
@Component({
  selector: "location",
  templateUrl: "location.html",
  providers: []
})
export class LocationComponent {
  @ViewChild("map") mapRef: ElementRef;
  segment: any;
  constructor(
    public platform: Platform,
    public modalCtrl: ModalController,
    public navCtrl: NavController
  ) {
    this.segment = "design";
    this.navCtrl = navCtrl;
  }

  goBack() {
    let modal = this.modalCtrl.create("HomeComponent");
    modal.present();
  }
  ionViewDidLoad() {
    console.log(this.mapRef);
    this.loadMap();
  }

  loadMap() {
    const location = new google.maps.LatLng(8.0057716,35.2630528,);

    const options = {
      center: location,
      streetViewControl: false,
      zoom: 4
    };

    var locations = [
      ['الرئاسة الخرطوم شارع الجمهورية تقاطع الجمهورية مع المك نمر', -15.606530, 32.535686 , 1],
      ['مجمع خدمات الجمهور بحرى', 15.636876, 32.538452 , 2],
      ['مجمع خدمات الجمهور امدرمان', 15.631005, 32.474272 , 3],
      ['الفحص الالى امدرمان', 15.672533, 32.491621 , 4],
      ['الفحص الالى جبرة', 15.527469, 32.537395 , 5],
      ['الفحص الالى شرق النيل', 15.599519, 32.606397 , 6],
      ['مكتب الرياض', 15.5777824,32.5713168 , 7],
      ['سنارالفحص الالى', 13.5632125,33.5747781 , 8],
      ['مدنى المكتب الرئيسى', 14.4002107,33.5257359 , 9],
      ['مكتب شندى  ', 16.690515,33.4313267 , 10],
      ['مكتب المتمة', 16.713780, 33.356595 , 11],
      ['مكتب عطبرة', 17.6908034,33.9797299 , 12],
      ['مكتب الابيض', 13.1854466,30.2167788 , 13],
      ['مكتب الدويم', 13.9937507,32.3134556 , 14],
      ['مكتب الحواتة', 13.418258, 34.627120 , 15]
    ];
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    const map = new google.maps.Map(this.mapRef.nativeElement, options);
    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

    this.addMarker(location, map);
  }
  addMarker(position, map) {
    return new google.maps.Marker({
      position,
      map
    });
  }
}
