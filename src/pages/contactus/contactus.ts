/** Represents a Component of contactus page. */

/** Imports Modules */
import { Component, OnInit } from "@angular/core";
import { IonicPage } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { ToastController, LoadingController } from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";

@IonicPage()
@Component({
  selector: "contactus",
  templateUrl: "contactus.html"
})
export class ContactUsComponent implements OnInit {
  myForm: FormGroup;
  userInfo = { name: "", email: "", phone: "", comment: "" };

  ApiMsg: { To: string; Cc: string; Subject: string } = {
    To: "ibrahim_elhussein@hotmail.com",
    Cc: "ibarhim@oasisoft.net",
    Subject: "Alnilein Mobile App - Contact Form"
  };

  constructor(
    public formBuilder: FormBuilder,
    public restProvider: RestProvider,
    private readonly loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {}

  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الرسالة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "خطأ في الإرسال, الرجاء اعادة المحاولة",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  ngOnInit(): any {
    this.myForm = this.formBuilder.group({
      name: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          this.nameValidator.bind(this)
        ]
      ],
      comment: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(300),
          this.commentValidator.bind(this)
        ]
      ],
      phone: ["", this.phoneValidator.bind(this)],
      email: ["", [Validators.required, this.emailValidator.bind(this)]]
    });
  }
  onSubmit() {
    let loader = this.loadingCtrl.create({
      spinner: "hide",
      content: `<img src="assets/img/loadin-gif.gif" />`,
      duration: 5000
    });
    loader.present();
    this.restProvider.addUser(this.ApiMsg, this.userInfo).then(
      result => {
        loader.dismiss();
        console.log(result);
        this.presentSuccessToast();
        this.userInfo = { name: "", email: "", phone: "", comment: "" };
        this.ngOnInit();
      },
      err => {
        loader.dismiss();
        this.presentErrorToast();
        console.log(err);
      }
    );
  }

  isValid(field: string) {
    let formField = this.myForm.get(field);
    return formField.valid || formField.pristine;
  }

  nameValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-]+$")) {
      return { invalidName: true };
    }
  }
  commentValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }
  phoneValidator(control: FormControl): { [s: string]: boolean } {
    if (control.value !== "") {
      if (
        !control.value.match(
          "^(?:[9\u0660-\u0669\u06F0-\u06F9]|(?:[0-9])){0,15}$"
        )
      ) {
        return { invalidPhone: true };
      }
    }
  }

  emailValidator(control: FormControl): { [s: string]: boolean } {
    if (
      !control.value
        .toLowerCase()
        .match(
          "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
        )
    ) {
      return { invalidEmail: true };
    }
  }
}
