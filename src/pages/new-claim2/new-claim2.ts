import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from "ionic-angular";
import { App } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { RestProvider } from "../../providers/rest/rest";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import {
  ToastController,
  LoadingController,
  MenuController
} from "ionic-angular";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { FileUploadOptions } from "@ionic-native/transfer";
import { Platform } from "ionic-angular";
/**
 * Generated class for the NewClaimPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-new2-claim",
  templateUrl: "new-claim2.html"
})
export class NewClaimPage2 implements OnInit {
  public photo1: any;
  public base64Image1: string;
  public buttonImage1: boolean = true;
  public photo2: any;
  public base64Image2: string;
  public buttonImage2: boolean = true;
  public photo3: any;
  public base64Image3: string;
  public buttonImage3: boolean = true;
  public photo4: any;
  public base64Image4: string;
  public buttonImage4: boolean = true;
  public photo5: any;
  public base64Image5: string;
  public buttonImage5: boolean = true;
  public photo6: any;
  public base64Image6: string;
  public buttonImage6: boolean = true;
  public photo7: any;
  public base64Image7: string;
  public buttonImage7: boolean = true;
  public photo8: any;
  public base64Image8: string;
  public buttonImage8: boolean = true;
  public error: string;
  public ClaimSNo: any;
  public UserClaimDate: any;
  public UserClaimLocation: any;
  public UserClaimComment: any;
  msgApi: any;
  ClientName: string;
  PolicyNo: string;
  PeriodFrom: string;
  PeriodTo: string;
  PlateNo: string;
  PolicyHolder: string;
  SNo: string;
  myForm: any;
  userInfo = { myDate: "", location: "", comment: "" };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public appCtrl: App,
    private storage: Storage,
    public formBuilder: FormBuilder,
    public restProvider: RestProvider,
    private alertCtrl: AlertController,
    private camera: Camera,
    private readonly loadingCtrl: LoadingController,
    private menu: MenuController,
    private readonly toastCtrl: ToastController,
    private transfer: FileTransfer,
    public platform: Platform
  ) {
    this.storage.get("UserClaimDate").then(UserClaimDate => {
      console.log("UserClaimDate: " + UserClaimDate);
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NewClaimPage");
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu
    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
  }
  Logout() {
    this.storage.set("LoggedIn", false);
    this.storage.clear();
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }
  GoBack() {
    this.storage.set("LoggedIn", true);
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }
  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال المطالبة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      this.storage
        .set("UserClaimComment", this.userInfo.comment)
        .then(UserClaimComment => {
          this.storage.set("ClaimSent", true).then(UserClaimComment => {
            this.navCtrl.parent.select(1);
            console.log("Dismissed toast");
          });
        });
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "خطأ في الإرسال, الرجاء اعادة المحاولة",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  presentSuccessToast2() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال المطالبة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      this.storage
        .set("UserClaimComment", this.userInfo.comment)
        .then(UserClaimComment => {
          this.storage.set("ClaimSent", true).then(UserClaimComment => {
            this.navCtrl.parent.select(1);
            console.log("Dismissed toast");
          });
        });
    });

    toast.present();
  }
  presentErrorToast2() {
    let toast = this.toastCtrl.create({
      message: "خطأ في الإرسال, الرجاء اعادة المحاولة",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  ngOnInit(): any {
    this.photo1 = [];
    this.buttonImage1 = true;
    this.photo2 = [];
    this.buttonImage2 = true;
    this.photo3 = [];
    this.buttonImage3 = true;
    this.photo4 = [];
    this.buttonImage4 = true;
    this.photo5 = [];
    this.buttonImage5 = true;
    this.photo6 = [];
    this.buttonImage6 = true;
    this.photo7 = [];
    this.buttonImage7 = true;
    this.photo8 = [];
    this.buttonImage8 = true;
    this.myForm = this.formBuilder.group({
      myDate: ["", [Validators.required, Validators.minLength(1)]],
      location: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(1000),
          this.locationValidator.bind(this)
        ]
      ],
      comment: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(1000),
          this.commentValidator.bind(this)
        ]
      ]
    });
  }

  isValid(field: string) {
    let formField = this.myForm.get(field);
    return formField.valid || formField.pristine;
  }

  locationValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }

  commentValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }

  myDateValidator(control: FormControl): { [s: string]: boolean } {
    if (
      !control.value.match(
        "/^(0[1-9]|1[0-2])/(0[1-9]|1d|2d|3[01])/(19|20)d{2}$/"
      )
    ) {
      return {};
    }
  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  getClaimSNO(claimSNo) {
    this.storage.set("ClaimSNo", claimSNo).then(ClientName => {});
  }

  onSubmit() {
    //  let loader = this.loadingCtrl.create({
    //    content: "Uploading..."
    //   });
    //   loader.present();
    let loader = this.loadingCtrl.create({
      spinner: "hide",
      content: `<img src="assets/img/loadin-gif.gif" /> <br/> جاري الارسال`,
      duration: 5000
    });
    loader.present();
    if (
      this.buttonImage1 === false ||
      this.buttonImage2 === false ||
      this.buttonImage3 === false ||
      this.buttonImage4 === false ||
      this.buttonImage5 === false ||
      this.buttonImage6 === false ||
      this.buttonImage7 === false ||
      this.buttonImage8 === false
    ) {
      console.log("With Image");
      this.storage.get("SNo").then(SNo => {
        console.log(SNo);
        this.SNo = SNo;
        this.storage.get("ClientName").then(ClientName => {
          console.log(ClientName);
          this.ClientName = ClientName;
          this.storage.get("PolicyNo").then(PolicyNo => {
            console.log(PolicyNo);
            this.PolicyNo = PolicyNo;
            this.storage.get("PeriodFrom").then(PeriodFrom => {
              console.log(PeriodFrom);
              this.PeriodFrom = PeriodFrom;
              this.storage.get("PeriodTo").then(PeriodTo => {
                console.log(PeriodTo);
                this.PeriodTo = PeriodTo;
                this.storage.get("PlateNo").then(PlateNo => {
                  console.log(PlateNo);
                  this.PlateNo = PlateNo;
                  this.storage.get("PolicyHolder").then(PolicyHolder => {
                    console.log(PolicyHolder);
                    this.PolicyHolder = PolicyHolder;
                    console.log("Policy Holder:" + this.PolicyHolder);
                    this.storage.get("UserClaimDate").then(UserClaimDate => {
                      this.UserClaimDate = UserClaimDate;
                      this.storage
                        .get("UserClaimLocation")
                        .then(UserClaimLocation => {
                          this.UserClaimLocation = UserClaimLocation;
                          this.storage
                            .get("UserClaimComment")
                            .then(UserClaimComment => {
                              this.UserClaimComment = UserClaimComment;
                              this.navCtrl.push("NewClaimPage2");

                              this.msgApi = {
                                ClientNo: this.SNo,
                                PolicyHolder: "" + this.PolicyHolder + "",
                                PolicyNo: "" + this.PolicyNo + "",
                                PlateNo: "" + this.PlateNo + "",
                                AccidentDate: "" + this.UserClaimDate + "",
                                AccidentLocation:
                                  "" + this.UserClaimLocation + "",
                                AccidentDetails:
                                  "" + this.UserClaimComment + "",
                                SubmittedBy: "" + this.ClientName + ""
                              };
                              this.platform.ready().then(() => {
                                this.restProvider
                                  .PostClaim(
                                    this.UserClaimDate,
                                    this.UserClaimLocation,
                                    this.UserClaimComment,
                                    this.msgApi
                                  )
                                  .then(
                                    result => {
                                      console.log(result);
                                      console.log("Claim SNo" + this.ClaimSNo);
                                      this.storage
                                        .set("ClaimSNo", result)
                                        .then(ClientName => {
                                          this.storage
                                            .get("ClaimSNo")
                                            .then(ClaimSNo => {
                                              this.ClaimSNo = ClaimSNo;
                                              this.uploadFile(ClaimSNo);
                                            });
                                        });
                                      loader.dismiss();
                                      //  this.presentSuccessToast();
                                      //
                                    },
                                    err => {
                                      this.presentErrorToast();
                                      console.log(err);
                                      loader.dismiss();
                                      //
                                    }
                                  );
                              });
                            });
                        });
                    });
                  });
                });
              });
            });
          });
        });
      });
    } else {
      console.log("Without Image");
      this.platform.ready().then(() => {
        this.storage.get("SNo").then(SNo => {
          console.log(SNo);
          this.SNo = SNo;
          this.storage.get("ClientName").then(ClientName => {
            console.log(ClientName);
            this.ClientName = ClientName;
            this.storage.get("PolicyNo").then(PolicyNo => {
              console.log(PolicyNo);
              this.PolicyNo = PolicyNo;
              this.storage.get("PeriodFrom").then(PeriodFrom => {
                console.log(PeriodFrom);
                this.PeriodFrom = PeriodFrom;
                this.storage.get("PeriodTo").then(PeriodTo => {
                  console.log(PeriodTo);
                  this.PeriodTo = PeriodTo;
                  this.storage.get("PlateNo").then(PlateNo => {
                    console.log(PlateNo);
                    this.PlateNo = PlateNo;
                    this.storage.get("PolicyHolder").then(PolicyHolder => {
                      console.log(PolicyHolder);
                      this.PolicyHolder = PolicyHolder;
                      console.log("Policy Holder:" + this.PolicyHolder);
                      this.storage.get("UserClaimDate").then(UserClaimDate => {
                        this.UserClaimDate = UserClaimDate;
                        this.storage
                          .get("UserClaimLocation")
                          .then(UserClaimLocation => {
                            this.UserClaimLocation = UserClaimLocation;
                            this.storage
                              .get("UserClaimComment")
                              .then(UserClaimComment => {
                                this.UserClaimComment = UserClaimComment;
                                this.navCtrl.push("NewClaimPage2");
                                this.msgApi = {
                                  ClientNo: this.SNo,
                                  PolicyHolder: "" + this.PolicyHolder + "",
                                  PolicyNo: "" + this.PolicyNo + "",
                                  PlateNo: "" + this.PlateNo + "",
                                  AccidentDate: "" + this.UserClaimDate + "",
                                  AccidentLocation:
                                    "" + this.UserClaimLocation + "",
                                  AccidentDetails:
                                    "" + this.UserClaimComment + "",
                                  SubmittedBy: "" + this.ClientName + ""
                                };
                                this.platform.ready().then(() => {
                                  this.restProvider
                                    .PostClaim(
                                      this.UserClaimDate,
                                      this.UserClaimLocation,
                                      this.UserClaimComment,
                                      this.msgApi
                                    )
                                    .then(
                                      result => {
                                        this.ClaimSNo = result;
                                        console.log(result);
                                        console.log(
                                          "Claim SNo" + this.ClaimSNo
                                        );
                                        loader.dismiss();
                                        this.Clear();
                                        //  this.presentSuccessToast();
                                        this.presentSuccessToast();
                                        //
                                      },
                                      err => {
                                        this.presentErrorToast();
                                        console.log(err);
                                        loader.dismiss();
                                        //
                                      }
                                    );
                                });
                              });
                          });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    }
  }

  takePhoto1() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage1 = false;
        this.base64Image1 = "data:image/jpeg;base64," + imageData;
        this.photo1.push(this.base64Image1);
        this.photo1.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto1(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage1 = true;
            this.photo1 = [];
            this.base64Image1 = "";
            this.photo1.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto2() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage2 = false;
        this.base64Image2 = "data:image/jpeg;base64," + imageData;
        this.photo2.push(this.base64Image2);
        this.photo2.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto2(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage2 = true;
            this.photo2 = [];
            this.base64Image2 = "";
            this.photo2.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto3() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage3 = false;
        this.base64Image3 = "data:image/jpeg;base64," + imageData;
        this.photo3.push(this.base64Image3);
        this.photo3.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto3(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage3 = true;
            this.photo3 = [];
            this.base64Image3 = "";
            this.photo3.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto4() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage4 = false;
        this.base64Image4 = "data:image/jpeg;base64," + imageData;
        this.photo4.push(this.base64Image4);
        this.photo4.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto4(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage4 = true;
            this.photo4 = [];
            this.base64Image4 = "";
            this.photo4.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto5() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage5 = false;
        this.base64Image5 = "data:image/jpeg;base64," + imageData;
        this.photo5.push(this.base64Image5);
        this.photo5.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto5(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage5 = true;
            this.photo5 = [];
            this.base64Image5 = "";
            this.photo5.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto6() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage6 = false;
        this.base64Image6 = "data:image/jpeg;base64," + imageData;
        this.photo6.push(this.base64Image6);
        this.photo6.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto6(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage6 = true;
            this.photo6 = [];
            this.base64Image6 = "";
            this.photo6.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto7() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage7 = false;
        this.base64Image7 = "data:image/jpeg;base64," + imageData;
        this.photo7.push(this.base64Image7);
        this.photo7.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto7(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage7 = true;
            this.photo7 = [];
            this.base64Image7 = "";
            this.photo7.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  takePhoto8() {
    const options: CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.buttonImage8 = false;
        this.base64Image8 = "data:image/jpeg;base64," + imageData;
        this.photo8.push(this.base64Image8);
        this.photo8.reverse();
      },
      err => {
        console.log(err);
      }
    );
  }
  deletePhoto8(index) {
    let confirm = this.alertCtrl.create({
      title: "هل ترغب بمسح الصورة ؟",
      message: "",
      buttons: [
        {
          text: "لا",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "نعم",
          handler: () => {
            console.log("Agree clicked");
            this.buttonImage8 = true;
            this.photo8 = [];
            this.base64Image8 = "";
            this.photo8.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  uploadFile(claimsNo) {
    let loader = this.loadingCtrl.create({
      spinner: "hide",
      content: `<img src="assets/img/loadin-gif.gif" style='text-align:center;' /> <br/> جاري ارسال الصور`,
      duration: 5000
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: "file",
      fileName: "image1.jpg",
      chunkedMode: false,
      headers: {}
    };
    fileTransfer
      .upload(
        this.photo1[0],
        "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" + claimsNo,
        options
      )
      .then(
        data => {
          console.log("Uploaded Image 1");
          const fileTransfer: FileTransferObject = this.transfer.create();
          let options: FileUploadOptions = {
            fileKey: "file",
            fileName: "image2.jpg",
            chunkedMode: false,
            headers: {}
          };
          fileTransfer
            .upload(
              this.photo2[0],
              "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                claimsNo,
              options
            )
            .then(
              data => {
                console.log("Uploaded Image 2");
                const fileTransfer: FileTransferObject = this.transfer.create();
                let options: FileUploadOptions = {
                  fileKey: "file",
                  fileName: "image3.jpg",
                  chunkedMode: false,
                  headers: {}
                };
                fileTransfer
                  .upload(
                    this.photo3[0],
                    "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                      claimsNo,
                    options
                  )
                  .then(
                    data => {
                      const fileTransfer: FileTransferObject = this.transfer.create();
                      let options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: "image4.jpg",
                        chunkedMode: false,
                        headers: {}
                      };
                      fileTransfer
                        .upload(
                          this.photo4[0],
                          "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                            claimsNo,
                          options
                        )
                        .then(
                          data => {
                            //
                            loader.dismiss();
                            this.uploadFile2(claimsNo);
                            console.log("Uploaded Image 3");
                            console.log(data + " Uploaded Successfully");
                            //
                            // this.presentToast("Image uploaded successfully");
                          },
                          err => {
                            loader.dismiss();
                            this.presentErrorToast2();
                            console.log(err);
                            //
                            //  this.presentToast(err);
                            this.uploadFile2(claimsNo);
                          }
                        );
                    },
                    err => {
                      //
                      //  this.presentToast(err);
                    }
                  );
              },
              err => {
                console.log("Uploaded Image 2");
                const fileTransfer: FileTransferObject = this.transfer.create();
                let options: FileUploadOptions = {
                  fileKey: "file",
                  fileName: "image3.jpg",
                  chunkedMode: false,
                  headers: {}
                };
                fileTransfer
                  .upload(
                    this.photo3[0],
                    "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                      claimsNo,
                    options
                  )
                  .then(
                    data => {
                      const fileTransfer: FileTransferObject = this.transfer.create();
                      let options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: "image4.jpg",
                        chunkedMode: false,
                        headers: {}
                      };
                      fileTransfer
                        .upload(
                          this.photo4[0],
                          "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                            claimsNo,
                          options
                        )
                        .then(
                          data => {
                            //
                            loader.dismiss();
                            this.uploadFile2(claimsNo);
                            console.log("Uploaded Image 3");
                            console.log(data + " Uploaded Successfully");
                            //
                            // this.presentToast("Image uploaded successfully");
                          },
                          err => {
                            //
                            //  this.presentToast(err);
                            this.uploadFile2(claimsNo);
                          }
                        );
                    },
                    err => {
                      const fileTransfer: FileTransferObject = this.transfer.create();
                      let options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: "image4.jpg",
                        chunkedMode: false,
                        headers: {}
                      };
                      fileTransfer
                        .upload(
                          this.photo4[0],
                          "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                            claimsNo,
                          options
                        )
                        .then(
                          data => {
                            //
                            loader.dismiss();
                            this.uploadFile2(claimsNo);
                            console.log("Uploaded Image 3");
                            console.log(data + " Uploaded Successfully");
                            //
                            // this.presentToast("Image uploaded successfully");
                          },
                          err => {
                            //
                            //  this.presentToast(err);
                            this.uploadFile2(claimsNo);
                          }
                        );
                      //
                      // this.presentToast(err);
                    }
                  );
              }
            );
        },
        err => {
          {
            console.log("Uploaded Image 1");
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
              fileKey: "file",
              fileName: "image2.jpg",
              chunkedMode: false,
              headers: {}
            };
            fileTransfer
              .upload(
                this.photo2[0],
                "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                  claimsNo,
                options
              )
              .then(
                data => {
                  console.log("Uploaded Image 2");
                  const fileTransfer: FileTransferObject = this.transfer.create();
                  let options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: "image3.jpg",
                    chunkedMode: false,
                    headers: {}
                  };
                  fileTransfer
                    .upload(
                      this.photo3[0],
                      "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                        claimsNo,
                      options
                    )
                    .then(
                      data => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image4.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo4[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              loader.dismiss();
                              this.uploadFile2(claimsNo);
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              //
                              //  this.presentToast(err);
                              this.uploadFile2(claimsNo);
                            }
                          );
                        //
                        // this.presentToast("Image uploaded successfully");
                      },
                      err => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image4.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo4[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              loader.dismiss();
                              this.uploadFile2(claimsNo);
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              //
                              //  this.presentToast(err);
                              this.uploadFile2(claimsNo);
                            }
                          );
                        //
                        //  this.presentToast(err);
                      }
                    );
                  // this.Clear();
                  console.log("Uploaded Image 2");
                  console.log(data + " Uploaded Successfully");
                  //
                  //  this.presentToast("Image uploaded successfully");
                },
                err => {
                  console.log("Uploaded Image 2");
                  const fileTransfer: FileTransferObject = this.transfer.create();
                  let options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: "image3.jpg",
                    chunkedMode: false,
                    headers: {}
                  };
                  fileTransfer
                    .upload(
                      this.photo3[0],
                      "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                        claimsNo,
                      options
                    )
                    .then(
                      data => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image4.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo4[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              loader.dismiss();
                              this.uploadFile2(claimsNo);
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              //
                              //  this.presentToast(err);
                              this.uploadFile2(claimsNo);
                            }
                          );
                        // this.presentToast("Image uploaded successfully");
                      },
                      err => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image4.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo4[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              loader.dismiss();
                              this.uploadFile2(claimsNo);
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              //
                              this.uploadFile2(claimsNo);
                              //  this.presentToast(err);
                            }
                          );
                        //  this.presentToast(err);
                      }
                    );
                  //  this.Clear();
                  console.log(err);
                  //
                  //   this.presentToast(err);
                }
              );
          }
        }
      );
  }

  uploadFile2(claimsNo) {
    let loader = this.loadingCtrl.create({
      spinner: "hide",
      content: `<img src="assets/img/loadin-gif.gif" style='text-align:center;' /> <br/> جاري ارسال الصور`,
      duration: 5000
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: "file",
      fileName: "image5.jpg",
      chunkedMode: false,
      headers: {}
    };
    fileTransfer
      .upload(
        this.photo5[0],
        "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" + claimsNo,
        options
      )
      .then(
        data => {
          console.log("Uploaded Image 1");
          const fileTransfer: FileTransferObject = this.transfer.create();
          let options: FileUploadOptions = {
            fileKey: "file",
            fileName: "image6.jpg",
            chunkedMode: false,
            headers: {}
          };
          fileTransfer
            .upload(
              this.photo6[0],
              "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                claimsNo,
              options
            )
            .then(
              data => {
                console.log("Uploaded Image 2");
                const fileTransfer: FileTransferObject = this.transfer.create();
                let options: FileUploadOptions = {
                  fileKey: "file",
                  fileName: "image7.jpg",
                  chunkedMode: false,
                  headers: {}
                };
                fileTransfer
                  .upload(
                    this.photo7[0],
                    "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                      claimsNo,
                    options
                  )
                  .then(
                    data => {
                      const fileTransfer: FileTransferObject = this.transfer.create();
                      let options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: "image8.jpg",
                        chunkedMode: false,
                        headers: {}
                      };
                      fileTransfer
                        .upload(
                          this.photo8[0],
                          "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                            claimsNo,
                          options
                        )
                        .then(
                          data => {
                            //
                            this.Clear();
                            loader.dismiss();
                            this.presentSuccessToast2();
                            console.log("Uploaded Image 3");
                            console.log(data + " Uploaded Successfully");
                            //
                            // this.presentToast("Image uploaded successfully");
                          },
                          err => {
                            this.Clear();
                            loader.dismiss();
                            this.presentSuccessToast2();
                            //
                            //  this.presentToast(err);
                          }
                        );
                    },
                    err => {
                      this.Clear();
                      loader.dismiss();
                      this.presentSuccessToast2();
                      //
                      //  this.presentToast(err);
                    }
                  );
              },
              err => {
                console.log("Uploaded Image 2");
                const fileTransfer: FileTransferObject = this.transfer.create();
                let options: FileUploadOptions = {
                  fileKey: "file",
                  fileName: "image7.jpg",
                  chunkedMode: false,
                  headers: {}
                };
                fileTransfer
                  .upload(
                    this.photo7[0],
                    "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                      claimsNo,
                    options
                  )
                  .then(
                    data => {
                      const fileTransfer: FileTransferObject = this.transfer.create();
                      let options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: "image8.jpg",
                        chunkedMode: false,
                        headers: {}
                      };
                      fileTransfer
                        .upload(
                          this.photo8[0],
                          "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                            claimsNo,
                          options
                        )
                        .then(
                          data => {
                            //
                            this.Clear();
                            loader.dismiss();
                            this.presentSuccessToast2();
                            console.log("Uploaded Image 3");
                            console.log(data + " Uploaded Successfully");
                            //
                            // this.presentToast("Image uploaded successfully");
                          },
                          err => {
                            this.Clear();
                            loader.dismiss();
                            this.presentSuccessToast2();
                            //
                            //  this.presentToast(err);
                          }
                        );
                    },
                    err => {
                      const fileTransfer: FileTransferObject = this.transfer.create();
                      let options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: "image8.jpg",
                        chunkedMode: false,
                        headers: {}
                      };
                      fileTransfer
                        .upload(
                          this.photo8[0],
                          "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                            claimsNo,
                          options
                        )
                        .then(
                          data => {
                            //
                            this.Clear();
                            loader.dismiss();
                            this.presentSuccessToast2();
                            console.log("Uploaded Image 3");
                            console.log(data + " Uploaded Successfully");
                            //
                            // this.presentToast("Image uploaded successfully");
                          },
                          err => {
                            this.Clear();
                            loader.dismiss();
                            this.presentSuccessToast2();
                            //
                            //  this.presentToast(err);
                          }
                        );
                      //
                      // this.presentToast(err);
                    }
                  );
              }
            );
        },
        err => {
          {
            console.log("Uploaded Image 1");
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
              fileKey: "file",
              fileName: "image6.jpg",
              chunkedMode: false,
              headers: {}
            };
            fileTransfer
              .upload(
                this.photo6[0],
                "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                  claimsNo,
                options
              )
              .then(
                data => {
                  console.log("Uploaded Image 2");
                  const fileTransfer: FileTransferObject = this.transfer.create();
                  let options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: "image7.jpg",
                    chunkedMode: false,
                    headers: {}
                  };
                  fileTransfer
                    .upload(
                      this.photo7[0],
                      "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                        claimsNo,
                      options
                    )
                    .then(
                      data => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image8.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo8[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              //
                              //  this.presentToast(err);
                            }
                          );
                        //
                        // this.presentToast("Image uploaded successfully");
                      },
                      err => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image8.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo8[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              //
                              //  this.presentToast(err);
                            }
                          );
                        //
                        //  this.presentToast(err);
                      }
                    );
                  // this.Clear();
                  console.log("Uploaded Image 2");
                  console.log(data + " Uploaded Successfully");
                  //
                  //  this.presentToast("Image uploaded successfully");
                },
                err => {
                  console.log("Uploaded Image 2");
                  const fileTransfer: FileTransferObject = this.transfer.create();
                  let options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: "image7.jpg",
                    chunkedMode: false,
                    headers: {}
                  };
                  fileTransfer
                    .upload(
                      this.photo7[0],
                      "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                        claimsNo,
                      options
                    )
                    .then(
                      data => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image8.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo8[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              //
                              //  this.presentToast(err);
                            }
                          );
                        // this.presentToast("Image uploaded successfully");
                      },
                      err => {
                        const fileTransfer: FileTransferObject = this.transfer.create();
                        let options: FileUploadOptions = {
                          fileKey: "file",
                          fileName: "image8.jpg",
                          chunkedMode: false,
                          headers: {}
                        };
                        fileTransfer
                          .upload(
                            this.photo8[0],
                            "http://elnileinapi.com/api/policies/claimImageUpload?SNo=" +
                              claimsNo,
                            options
                          )
                          .then(
                            data => {
                              //
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              console.log("Uploaded Image 3");
                              console.log(data + " Uploaded Successfully");
                              //
                              // this.presentToast("Image uploaded successfully");
                            },
                            err => {
                              this.Clear();
                              loader.dismiss();
                              this.presentSuccessToast2();
                              //
                              //  this.presentToast(err);
                            }
                          );
                        //  this.presentToast(err);
                      }
                    );
                  //  this.Clear();
                  console.log(err);
                  //
                  //   this.presentToast(err);
                }
              );
          }
        }
      );
  }

  Clear() {
    this.ngOnInit();
    this.photo1 = [];
    this.photo2 = [];
    this.photo3 = [];
    this.photo4 = [];
    this.photo5 = [];
    this.photo6 = [];
    this.photo7 = [];
    this.photo8 = [];
    this.base64Image1 = "";
    this.base64Image2 = "";
    this.base64Image3 = "";
    this.base64Image4 = "";
    this.base64Image5 = "";
    this.base64Image6 = "";
    this.base64Image7 = "";
    this.base64Image8 = "";
    this.buttonImage1 = true;
    this.buttonImage2 = true;
    this.buttonImage3 = true;
    this.buttonImage4 = true;
    this.buttonImage5 = true;
    this.buttonImage6 = true;
    this.buttonImage7 = true;
    this.buttonImage8 = true;
    this.userInfo = { myDate: "", location: "", comment: "" };
  }
}
