import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewClaimPage2 } from './new-claim2';

@NgModule({
  declarations: [
    NewClaimPage2,
  ],
  imports: [
    IonicPageModule.forChild(NewClaimPage2),
  ],
})
export class NewClaimPage2Module {}
