import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";

/**
 * Generated class for the ClaimTutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-claim-tutorial",
  templateUrl: "claim-tutorial.html"
})
export class ClaimTutorialPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private view: ViewController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad ClaimTutorialPage");
  }
  closeModal() {
    this.view.dismiss();
  }
}
