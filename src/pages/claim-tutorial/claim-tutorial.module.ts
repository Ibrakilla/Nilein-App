import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClaimTutorialPage } from './claim-tutorial';

@NgModule({
  declarations: [
    ClaimTutorialPage,
  ],
  imports: [
    IonicPageModule.forChild(ClaimTutorialPage),
  ],
})
export class ClaimTutorialPageModule {}
