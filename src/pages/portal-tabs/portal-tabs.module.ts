import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PortalTabsPage } from './portal-tabs';

@NgModule({
  declarations: [
    PortalTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(PortalTabsPage),
  ]
})
export class PortalTabsPageModule {}
