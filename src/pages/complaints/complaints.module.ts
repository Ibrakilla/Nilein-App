import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { ComplaintsComponent } from './complaints';
@NgModule({
  declarations: [
    ComplaintsComponent,
  ],
  imports: [
    IonicPageModule.forChild(ComplaintsComponent),
    TranslateModule
  ],
  exports: [
    ComplaintsComponent,
    TranslateModule
  ]
})
export class ComplaintsModule {}
