import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
/*
  Generated class for the StorageServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageServiceProvider {
  constructor(public http: Http, private storage: Storage) {
    console.log("Hello Localstorage Providerrrrrrrrrrr");
  }

  PolicyInfo(
    SNo,
    PolicyNo,
    PolicyHolder,
    ClientName,
    PlateNo,
    PeriodFrom,
    PeriodTo
  ) {
    this.storage.set("LoggedIn", true);
    this.storage.set("SNo", SNo);
    this.storage.set("ClientName", ClientName);
    this.storage.set("PolicyHolder", PolicyHolder);
    this.storage.set("PlateNo", PlateNo);
    this.storage.set("PolicyNo", PolicyNo);
    this.storage.set("PeriodFrom", PeriodFrom);
    this.storage.set("PeriodTo", PeriodTo);
  }
  //store the email address
  setEmail(email) {
    this.storage.set("email", email);
  }

  //get the stored email
  getEmail() {
    this.storage.get("email").then(email => {
      console.log("email: " + email);
    });
  }

  //delete the email address
  removeEmail() {
    this.storage.remove("email").then(() => {
      console.log("email is removed");
    });
  }

  //clear the whole local storage
  clearStorage() {
    this.storage.clear().then(() => {
      console.log("all keys are cleared");
    });
  }
}
