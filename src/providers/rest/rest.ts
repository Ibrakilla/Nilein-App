import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  apiEmail = "http://elnileinapi.com/api/email/PostComplaint";
  apiLogin = "http://elnileinapi.com/api/policies/getpolicy";
  apiClaim = "http://elnileinapi.com/api/policies/postnewclaim";
  apiClaimImage = "http://elnileinapi.com/api/policies/claimImageUpload?SNo=13";
  apiUserPendingClaims = "http://localhost:24575/api/policies/getclaims";
  apiUserClaims = "http://elnileinapi.com/api/policies/GetPendingClaims";
  constructor(public http: HttpClient) {
    console.log("Hello RestProvider Provider");
  }
  ApiMsg: any;
  msgApi: any;
  ClientName: string;
  PolicyNo: string;
  PeriodFrom: string;
  PeriodTo: string;
  PlateNo: string;
  PolicyHolder: string;
  SNo: string;
  addUser(data2, data) {
    this.msgApi =
      "Name: " +
      data.name +
      "<br />" +
      "Email: " +
      data.email +
      "<br />" +
      "Phone: " +
      data.phone +
      "<br />" +
      "Comment: " +
      data.comment;

    this.ApiMsg = {
      To: "ibrahim_elhussein@hotmail.com",
      Cc: "info@oasisoft.net",
      Subject: "Elnilein Mobile App - Contact Form",
      Body: this.msgApi
    };

    console.log(data);
    console.log(data2);
    console.log(this.msgApi);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.apiEmail, this.ApiMsg, {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  addUser2(data2, data) {
    this.msgApi =
      "Name: " +
      data.name +
      "<br />" +
      "Email: " +
      data.email +
      "<br />" +
      "Phone: " +
      data.phone +
      "<br />" +
      "Notes: " +
      data.comment;

    this.ApiMsg = {
      To: "ibrahim_elhussein@hotmail.com",
      Cc: "info@oasisoft.net",
      Subject: "Elnilein Mobile App - Complaints Form",
      Body: this.msgApi
    };

    console.log(data);
    console.log(data2);
    console.log(this.msgApi);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.apiEmail, this.ApiMsg, {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  login(data, data2) {
    console.log(data.policyno);
    JSON.stringify(data);
    return new Promise((resolve, reject) => {
      this.http
        .get(this.apiLogin + "?policyno=" + data + "&plateno=" + data2 + "", {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            console.log(res);
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  PostClaim(AccidentDate, AccidentLocation, AccidentDetails, msgApi) {
    console.log(msgApi);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.apiClaim, msgApi, {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  postData(credentials) {
    console.log(credentials);
    return new Promise((resolve, reject) => {
      this.http.post(this.apiClaimImage, credentials).subscribe(
        res => {
          resolve(res);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  UserClaims(clientno) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.apiUserClaims + "?ClientNo=" + clientno, {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            console.log(res);
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  UserPendingClaims(clientno, policyno) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.apiUserPendingClaims +
            "?ClientID=" +
            clientno +
            "&PolicyNo=" +
            policyno +
            "",
          {
            headers: new HttpHeaders().set("Content-Type", "application/json")
          }
        )
        .subscribe(
          res => {
            console.log(res);
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }
}
